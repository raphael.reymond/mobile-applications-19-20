package mobiledev.heia.ch;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

public class SimpleUpActivity extends AppCompatActivity {
    private static final String TAG = SimpleUpActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_up);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        Log.w(TAG, "Simple onCreate");
    }
    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.activity_simple_up);
        Log.w(TAG, "Simple onStart");
    }
    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.activity_simple_up);
        Log.w(TAG, "Simple onResume");
    }
    @Override
    protected void onPause() {
        super.onPause();
        setContentView(R.layout.activity_simple_up);
        Log.w(TAG, "Simple onPause");
    }
    @Override
    protected void onStop() {
        super.onStop();
        setContentView(R.layout.activity_simple_up);
        Log.w(TAG, "Simple onStop");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        setContentView(R.layout.activity_simple_up);
        Log.w(TAG, "Simple onDestroy");
    }
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        setContentView(R.layout.activity_simple_up);
        Log.w(TAG, "Simple onSaveInstanceState");
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        setContentView(R.layout.activity_simple_up);
        Log.w(TAG, "Simple onRestoreInstanceState");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
