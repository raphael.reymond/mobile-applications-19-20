package mobiledev.heia.ch;

        import androidx.appcompat.app.AppCompatActivity;

        import android.content.Intent;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;

public class LaunchActivity extends AppCompatActivity {
    private static final String TAG = LaunchActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        Log.w(TAG, "Launch onCreate");
    }
    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.activity_launch);
        Log.w(TAG, "Launch onStart");
    }
    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.activity_launch);
        Log.w(TAG, "Launch onResume");
    }
    @Override
    protected void onPause() {
        super.onPause();
        setContentView(R.layout.activity_launch);
        Log.w(TAG, "Launch onPause");
    }
    @Override
    protected void onStop() {
        super.onStop();
        setContentView(R.layout.activity_launch);
        Log.w(TAG, "Launch onStop");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        setContentView(R.layout.activity_launch);
        Log.w(TAG, "Launch onDestroy");
    }
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        setContentView(R.layout.activity_launch);
        Log.w(TAG, "Launch onSaveInstanceState");
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        setContentView(R.layout.activity_launch);
        Log.w(TAG, "Launch onRestoreInstanceState");
    }
    public void onLaunchActivity(View v) {
        Intent intent = new Intent(this, SimpleUpActivity.class);
        startActivity(intent);
    }
    public void onLaunchPeerActivity(View v) {
        Intent intent = new Intent(this, PeerActivity.class);
        intent.putExtra("EXTRA_PEER_COUNT", "1"); //Create intent to launch activity with the counter
        startActivity(intent);
    }
}
