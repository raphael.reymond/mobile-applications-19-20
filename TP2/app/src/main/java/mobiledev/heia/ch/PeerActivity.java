package mobiledev.heia.ch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class PeerActivity extends AppCompatActivity {
    private static final String TAG = PeerActivity.class.getSimpleName();
    private String sessionId;
    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peer);
        sessionId = getIntent().getStringExtra("EXTRA_PEER_COUNT"); //recuperate the intent
        count = Integer.parseInt(sessionId); //convert intent in int
        String newText = String.format(getString(R.string.peer_count),String.valueOf(count)); //Write string with activity number
        TextView titleEdit = findViewById(R.id.counter);
        titleEdit.setText(newText); //write the number of activity
    }

    public void ButtonPeerActivity(View v) {
        count++; //increment activity
        Intent intent = new Intent(this, PeerActivity.class); //send counter
        intent.putExtra("EXTRA_PEER_COUNT", Integer.toString(count));
        startActivity(intent); //start activity with the intent
    }
}
