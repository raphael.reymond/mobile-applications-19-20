package mobiledev.heia.ch;

import androidx.lifecycle.ViewModel;

public class PersonNameViewModel extends ViewModel {
    private String mName;

    public void setName(String name){
        this.mName = name;
    }
    public String getName(){
       return this.mName;
    }
}
