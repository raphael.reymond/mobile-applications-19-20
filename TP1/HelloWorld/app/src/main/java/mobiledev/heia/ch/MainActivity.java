package mobiledev.heia.ch;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private final static  String STRING_STATE = MainActivity.class.getSimpleName() + ".STRING_STATE";

    private PersonNameViewModel mVviewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.w(TAG, "Application Ready");

        mVviewModel = new ViewModelProvider(this).get(PersonNameViewModel.class);
        if(mVviewModel != null && mVviewModel.getName() == null){
            String name = getResources().getString(R.string.title_base);
            mVviewModel.setName(name);
        }
        Log.w(TAG, "Test msg 2" + mVviewModel.getName());
    }
    public void onClickDisplay(View v) {
        EditText editName = findViewById(R.id.EnterText);
        TextView titleEdit = findViewById(R.id.Title) ;

        String name = editName.getText().toString();
        String newText = String.format(getResources().getString(R.string.newText), name);
        titleEdit.setText(newText);
        mVviewModel.setName(name);
        Log.w(TAG, "Test msg 1" + mVviewModel.getName());
    }
    public void onClickClear(View v) {
        String newText = String.format(getString(R.string.newText), getString(R.string.title_base));
        TextView titleEdit = findViewById(R.id.Title);
        titleEdit.setText(newText);
        Log.w(TAG, "clear");
    }
}
