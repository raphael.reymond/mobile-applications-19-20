package ch.heia.mobiledev.launchactivity.viewModel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;
import java.util.List;

import ch.heia.mobiledev.launchactivity.EntryRepository;
import ch.heia.mobiledev.launchactivity.ui.LaunchActivityWithNav;
import ch.heia.mobiledev.launchactivity.data.Entry;
import ch.heia.mobiledev.launchactivity.network.FetchAsyncTask;
import ch.heia.mobiledev.launchactivity.network.Response;

@SuppressWarnings("ALL")
public class ListNoAcloolViewModel extends AndroidViewModel {
  // data members
  private final EntryRepository mRepository;
  private final LiveData<List<Entry>> listNoAlcohol;
  private static final String TAG = LaunchActivityWithNav.class.getSimpleName();
  // for observation by the UI component
  private final MutableLiveData<Response> mResponse = new MutableLiveData<>();

  // constructor
  public ListNoAcloolViewModel(Application application) {
    super(application);
    mRepository = new EntryRepository(application);
    this.listNoAlcohol = mRepository.getAllCocktailsNoAlcohol();
  }

  @SuppressWarnings("WeakerAccess")
  public class EntryViewModelFactory extends ViewModelProvider.AndroidViewModelFactory {
    // data members
    private final Application mApplication;

    public EntryViewModelFactory(Application application) {
      super(application);
      mApplication = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
      //noinspection unchecked
      return (T) new ListNoAcloolViewModel(mApplication);
    }
  }

  // method called for fetching the data
  public void fetchData(LifecycleOwner lifecycleOwner, String path, String param, String value) {
    // async task used for fetching the data
    FetchAsyncTask fetchAsyncTask = new FetchAsyncTask(path, param, value);
    // observe results from the async task
    fetchAsyncTask.getResponse().observe(lifecycleOwner, response -> {
      Entry entry = response.getEntry(0);
      Log.d(TAG, "salut " + entry);
      Log.d(TAG, "Response received");
      mResponse.postValue(response);
    });

      fetchAsyncTask.execute();

  }

  // list of news entry to be observed by UI
  public LiveData<Response> getResponse() {
    return mResponse;
  }

  // for accessing a specific entry
  public Entry getEntry(int index) {
    return listNoAlcohol.getValue().get(index);
  }

  // getter methods for live data
  public LiveData<List<Entry>> getListNoAlcohol() { return mRepository.getAllCocktailsNoAlcohol(); }

  public void setName(String id, String name, ArrayList<String> ing, ArrayList<String> measure, String imgPath, String info, String alcohol) {
    Log.d(TAG, "Name set to " + id);
    // add the name to the repository
    mRepository.insert(new Entry(id, name, ing, measure, imgPath, info,  alcohol));
  }
}
