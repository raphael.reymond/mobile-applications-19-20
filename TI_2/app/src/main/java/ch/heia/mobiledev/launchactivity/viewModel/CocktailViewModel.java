package ch.heia.mobiledev.launchactivity.viewModel;

import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import ch.heia.mobiledev.launchactivity.ui.LaunchActivityWithNav;
import ch.heia.mobiledev.launchactivity.data.Entry;
import ch.heia.mobiledev.launchactivity.network.FetchAsyncTask;
import ch.heia.mobiledev.launchactivity.network.Response;

public class CocktailViewModel extends ViewModel {
    // data members


    // used for logging
    private static final String TAG = LaunchActivityWithNav.class.getSimpleName();

    // for observation by the UI component
    private final MutableLiveData<Response> mResponse = new MutableLiveData<>();

    // constructor
    public CocktailViewModel() {
    }

    // method called for fetching the data
    public void fetchData(LifecycleOwner lifecycleOwner, String path, String param, String value) {
        // async task used for fetching the data
        FetchAsyncTask fetchAsyncTask = new FetchAsyncTask(path, param, value);
        // observe results from the async task
        fetchAsyncTask.getResponse().observe(lifecycleOwner, response -> {
            Entry entry = response.getEntry(0);
            Log.d(TAG, "salut " + entry);
            Log.d(TAG, "Response received");
            mResponse.postValue(response);
        });

        fetchAsyncTask.execute();

    }

    // list of news entry to be observed by UI
    public LiveData<Response> getResponse() {
        return mResponse;
    }

}
