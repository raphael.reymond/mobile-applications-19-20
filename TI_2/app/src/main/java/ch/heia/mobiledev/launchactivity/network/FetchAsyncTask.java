package ch.heia.mobiledev.launchactivity.network;

import android.os.AsyncTask;

import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import ch.heia.mobiledev.launchactivity.data.Entry;

// the generic types of your own FetchAsyncTask may vary based on your implementation
public class FetchAsyncTask extends AsyncTask<Void, Void, Response> {
  // data members/fields initialized upon construction
  // data fields may store specific parameters for building the appropriate 
  // URL depending on the web API

  private final String mParam;
  private final String mValue;
  private final String mPath;
  
  // for observation of the results by the owner of the instance of FetchAsyncTask
  // the observation mechanism uses LiveData
  private final MutableLiveData<Response> mResponse = new MutableLiveData<>();
  
  // constructor
  public FetchAsyncTask(String path, String param, String value) {
    this.mParam = param;
    this.mValue = value;
    this.mPath = path;
  }
  
  // for observation by the view model
  public LiveData<Response> getResponse() {
    return mResponse;
  }

  @Override
  protected Response doInBackground(Void... voids) {
    // To be implemented
    Response response = null;
    URL url = NetworkUtils.getUrl(mPath, mParam, mValue);
    try{
      String stringResponse = NetworkUtils.getResponseFromHttpUrl(url);
      if(stringResponse!=null){
        response = JsonParser.parse(stringResponse);
      }
    } catch (IOException | JSONException e){
      e.printStackTrace();
    }
    return response;
  }

  @Override
  protected void onPostExecute(Response response) {
    System.out.println("test" + response);
    if(response == null){
      List<Entry> nulResult = new ArrayList<>(1);

      Entry emptyEntry = new Entry("0", "No results found", "", "");
      nulResult.add(0,emptyEntry);
      response = new Response(nulResult);
    }
    mResponse.setValue(response);
  }
}
