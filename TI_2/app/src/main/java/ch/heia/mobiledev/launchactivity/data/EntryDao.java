package ch.heia.mobiledev.launchactivity.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface EntryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    // method to add entry en db
    void insert(Entry cocktail);
    @Query("SELECT * from entry_table")
    LiveData<List<Entry>> getAllCocktails();

    @Query("SELECT * from entry_table WHERE Alcohol = :alc")
    LiveData<List<Entry>> getAllCocktailsWithAlcohol(String alc);

    @Query("SELECT * from entry_table WHERE Alcohol = :alc")
    LiveData<List<Entry>> getAllCocktailsNoAlcohol(String alc);

    @Query("SELECT * from entry_table WHERE Name LIKE :alc")
    LiveData<List<Entry>> getAllCocktailsName(String alc);

    @Query("SELECT * from entry_table WHERE ingredient IN (SELECT ingredient from entry_table WHERE name LIKE :alc)")
    LiveData<List<Entry>> getAllCocktailsIngredient(String alc);
    @Query("DELETE FROM entry_table")
    void deleteTable();
}