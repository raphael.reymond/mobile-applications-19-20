package ch.heia.mobiledev.launchactivity.network;

import androidx.annotation.NonNull;

import java.util.List;

import ch.heia.mobiledev.launchactivity.data.Entry;

public class Response {
  // data members
  // array of entries
  @NonNull
  private final List<Entry> mEntries;

  public Response(@NonNull final List<Entry> entries) {
    mEntries = entries;
  }

  public List<Entry> getEntries() {
    return mEntries;
  }
  public Entry getEntry(int index) {
    return mEntries.get(index);
  }
}
