package ch.heia.mobiledev.launchactivity.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import ch.heia.mobiledev.launchactivity.R;
import ch.heia.mobiledev.launchactivity.ui.SearchByIngredientDirections;

@SuppressWarnings("WeakerAccess")
public class SearchByIngredient extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.searchebyingredientfragment, container, false);
    }
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button buttonSearch = view.findViewById(R.id.search_button);
        EditText editName  = view.findViewById(R.id.editTextCocktailName);

        // preapre the parameters for the network request
        String[] params = {"filter.php", "i", ""};
        buttonSearch.setOnClickListener(v -> {
            // get all coktails which the asked ingredient and set it to params array
            params[2] = editName.getText().toString();
            SearchByIngredientDirections.ActionSearchByIngredientToListCocktailFragment action = SearchByIngredientDirections.actionSearchByIngredientToListCocktailFragment(params);
            action.setParam(params);
            Navigation.findNavController(view).navigate(action);
        });
    }
}