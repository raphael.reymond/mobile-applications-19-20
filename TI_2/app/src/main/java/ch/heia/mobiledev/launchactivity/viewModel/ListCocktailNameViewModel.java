package ch.heia.mobiledev.launchactivity.viewModel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;
import java.util.List;

import ch.heia.mobiledev.launchactivity.EntryRepository;
import ch.heia.mobiledev.launchactivity.ui.LaunchActivityWithNav;
import ch.heia.mobiledev.launchactivity.data.Entry;
import ch.heia.mobiledev.launchactivity.network.FetchAsyncTask;
import ch.heia.mobiledev.launchactivity.network.Response;

public class ListCocktailNameViewModel extends AndroidViewModel {
  // data members
  private final EntryRepository mRepository;
  private static final String TAG = LaunchActivityWithNav.class.getSimpleName();

  // for observation by the UI component
  private final MutableLiveData<Response> mResponse = new MutableLiveData<>();

  // constructor
  public ListCocktailNameViewModel(Application application) {
    super(application);
    this.mRepository = new EntryRepository(application);
  }

  @SuppressWarnings({"WeakerAccess", "unused", "InnerClassMayBeStatic"})
  public class EntryViewModelFactory extends ViewModelProvider.AndroidViewModelFactory implements ViewModelProvider.Factory{
    // data members
    private final Application mApplication;

    public EntryViewModelFactory(Application application) {
      super(application);
      mApplication = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
      //noinspection unchecked
      return (T) new ListCocktailNameViewModel(mApplication);
    }
  }

  // method called for fetching the data
  public void fetchData(LifecycleOwner lifecycleOwner, String path, String param, String value) {
    // async task used for fetching the data
    FetchAsyncTask fetchAsyncTask = new FetchAsyncTask(path, param, value);
    // observe results from the async task
    fetchAsyncTask.getResponse().observe(lifecycleOwner, response -> {
      Entry entry = response.getEntry(0);
      Log.d(TAG, "salut " + entry);
      Log.d(TAG, "Response received");
      mResponse.postValue(response);
    });

      fetchAsyncTask.execute();
  }

  // list of news entry to be observed by UI
  public LiveData<Response> getResponse() {
    return mResponse;
  }

  // getter methods for live data
  public LiveData<List<Entry>> getAllCocktailsName(String mValue) { return mRepository.getAllCocktailsListName(mValue); }

  public LiveData<List<Entry>> getAllCocktailsIngredient(String mValue) {return mRepository.getAllCocktailsListIngredient(mValue);
  }

  public void setName(String id, String name, ArrayList<String> ing, ArrayList<String> measure, String imgPath, String info, String alcohol) {
    Log.d(TAG, "Name set to " + id);
    // add the name to the repository
    mRepository.insert(new Entry(id, name, ing, measure, imgPath, info, alcohol));
  }
}
