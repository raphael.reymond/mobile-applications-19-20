package ch.heia.mobiledev.launchactivity.network;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ch.heia.mobiledev.launchactivity.data.Entry;

class JsonParser {
  // data members
  // define constant strings used for parsing the JSON response received from the web API
  private static final String STR_DRINK = "strDrink";
  private static final String STR_DRINK_Thumb = "strDrinkThumb";
  private static final String ID_DRINK = "idDrink";
  private static final String STR_INSTRUCTION = "strInstructions";
  private static final String STR_ING1 = "strIngredient1";
  private static final String STR_ING2 = "strIngredient2";
  private static final String STR_ING3 = "strIngredient3";
  private static final String STR_ING4 = "strIngredient4";
  private static final String STR_MEAS1 = "strMeasure1";
  private static final String STR_MEAS2 = "strMeasure2";
  private static final String STR_MEAS3 = "strMeasure3";
  private static final String STR_MEAS4 = "strMeasure4";
  private static final ArrayList<String> listIngredient = new ArrayList<>();
  private static final ArrayList<String> listMeasure = new ArrayList<>();
  @SuppressWarnings("FieldCanBeLocal")
  private static Entry entry;

  // method called for parsing the response
  // responseStr is the JSON response stored in a String
  public static Response parse(final String responseStr) throws JSONException {
    // first get a JSON object from the received string
    JSONObject object = new JSONObject(responseStr);

    // then get all entries from the jsonObject
    List<Entry> entries = entriesFromJson(object);

    // return the response that contains all entries
    //noinspection UnnecessaryLocalVariable
    Response response = new Response(entries);
    return response;
  }

  // this method receives the JSON object representing the response received from the web API
  @NonNull
  private static List<Entry> entriesFromJson(final JSONObject json) throws JSONException {

    // get the JSON array representing all entries (e.g. one entry represents one weather forecast
    // entry among all weather forecasts)
    JSONArray array = json.getJSONArray("drinks");

    // allocate the array for all news entries
    List<Entry> entries = new  ArrayList<>(array.length());

    // get all entries from the JSON array
    for (int i = 0; i < array.length(); i++) {
      try{
        // Get the JSON object representing the specific entry
        JSONObject oneObject = array.getJSONObject(i);

        // from this JSON object get the entry
        String id = oneObject.getString(ID_DRINK);
        String image = oneObject.getString(STR_DRINK_Thumb);
        String name = oneObject.getString(STR_DRINK);
        if(!oneObject.isNull(STR_INSTRUCTION)){
          listIngredient.clear();
          listMeasure.clear();
          String info = oneObject.getString(STR_INSTRUCTION);
          listIngredient.add(oneObject.getString(STR_ING1));
          listIngredient.add(oneObject.getString(STR_ING2));
          listIngredient.add(oneObject.getString(STR_ING3));
          listIngredient.add(oneObject.getString(STR_ING4));

          listMeasure.add(oneObject.getString(STR_MEAS1));
          listMeasure.add(oneObject.getString(STR_MEAS2));
          listMeasure.add(oneObject.getString(STR_MEAS3));
          listMeasure.add(oneObject.getString(STR_MEAS4));

          entry = new Entry(id, name, listIngredient, listMeasure, image, info, "");
        }
        else {
          entry = new Entry(id, name, image, "");
        }

        entries.add(i, entry);
      } catch (JSONException e){
        e.printStackTrace();
      }
    }
    // return the array of entries
    return entries;
  }

}
