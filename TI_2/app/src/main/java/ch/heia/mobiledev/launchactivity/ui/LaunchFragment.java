package ch.heia.mobiledev.launchactivity.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import ch.heia.mobiledev.launchactivity.R;

@SuppressWarnings("WeakerAccess")
public class LaunchFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.launch_fragment, container, false);
        setHasOptionsMenu(true);
        return view;
    }
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

            Button buttonSearch = view.findViewById(R.id.search_button);
            EditText editName  = view.findViewById(R.id.editTextCocktailName);

            // preapre the parameters for the network request
            String[] params = {"search.php", "s", ""};
            buttonSearch.setOnClickListener(v -> {
                // get cocktail which is asked by the user and set it to params array
                params[2] = editName.getText().toString();
                LaunchFragmentDirections.ActionLaunchDestToListCocktailFragment action = LaunchFragmentDirections.actionLaunchDestToListCocktailFragment(params);
                action.setParam(params);
                Navigation.findNavController(view).navigate(action);
            });
    }
}
