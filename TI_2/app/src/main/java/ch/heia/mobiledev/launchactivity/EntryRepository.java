package ch.heia.mobiledev.launchactivity;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import ch.heia.mobiledev.launchactivity.data.Entry;
import ch.heia.mobiledev.launchactivity.data.EntryDao;
import ch.heia.mobiledev.launchactivity.data.EntryRoomDatabase;

@SuppressWarnings("ALL")
public class EntryRepository {
    private final EntryDao mEntryDao;
    private final LiveData<List<Entry>> mAllCocktails;
    private final LiveData<List<Entry>> mAllCocktailsWithAlcohol;
    private final LiveData<List<Entry>> mAllCocktailsNoAlcohol;

    public EntryRepository(Application application) {
        EntryRoomDatabase db = EntryRoomDatabase.getDatabase(application);
        mEntryDao = db.entryDao();
        mAllCocktails = mEntryDao.getAllCocktails();
        mAllCocktailsWithAlcohol = mEntryDao.getAllCocktailsWithAlcohol("Alcoholic");
        mAllCocktailsNoAlcohol = mEntryDao.getAllCocktailsNoAlcohol("Non_Alcoholic");
    }

    @SuppressWarnings("unused")
    public LiveData<List<Entry>> getAllCocktails() {
        return mAllCocktails;
    }

    public LiveData<List<Entry>> getAllCocktailsWithAlcohol() {
        return this.mAllCocktailsWithAlcohol;
    }
    public LiveData<List<Entry>> getAllCocktailsNoAlcohol() {
        return mAllCocktailsNoAlcohol;
    }

    public LiveData<List<Entry>> getAllCocktailsListName(String s) {
        return mEntryDao.getAllCocktailsName(s);
    }
    public LiveData<List<Entry>> getAllCocktailsListIngredient(String s) {
        return mEntryDao.getAllCocktailsIngredient(s);
    }
    public void deleteData(){
        mEntryDao.deleteTable();
    }

    public void insert(Entry cocktail) {
        new insertAsyncTask(mEntryDao).execute(cocktail);
    }

    private static class insertAsyncTask extends AsyncTask<Entry, Void, Void> {

        private final EntryDao mAsyncTaskDao;

        insertAsyncTask(EntryDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Entry... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
