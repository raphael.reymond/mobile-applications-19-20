package ch.heia.mobiledev.launchactivity.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import ch.heia.mobiledev.launchactivity.R;

public class LaunchActivityWithNav extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
  // data members
  private final static String TAG = LaunchActivityWithNav.class.getSimpleName();
    private AppBarConfiguration mAppbar;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Log.d(TAG, "onCreate()");
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    NavController navController = Navigation.findNavController(this, R.id.fragment_container);
      DrawerLayout drawer = findViewById(R.id.drawer_layout);
    mAppbar = new AppBarConfiguration.Builder(navController.getGraph())
                    .setDrawerLayout(drawer)
                    .build();
    NavigationView navigationView = findViewById(R.id.nav_view);
    NavigationUI.setupActionBarWithNavController(this, navController, mAppbar);
    NavigationUI.setupWithNavController(navigationView, navController);

  }
  @Override
  public boolean onSupportNavigateUp() {
    NavController navController = Navigation.findNavController(this, R.id.fragment_container);
    return NavigationUI.navigateUp(navController, mAppbar)
            || super.onSupportNavigateUp();
  }

  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    return false;
  }
}