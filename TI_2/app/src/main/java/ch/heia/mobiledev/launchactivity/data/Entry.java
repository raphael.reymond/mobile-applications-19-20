package ch.heia.mobiledev.launchactivity.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.ArrayList;

// create table in db
@SuppressWarnings("WeakerAccess")
@Entity(tableName ="entry_table", indices = {@Index(value = {"id"}, unique = true)})

// an entry represent a cocktail in our application
public class Entry implements Serializable {
  // data members/fields
  // each data member/field represents a field in each piece of data
  // received from the web API
  // choose the appropriate modifiers for each field
  
  
  // constructor
  // each data field must be initialized correctly upon construction
  // based on data received from the web API
  @PrimaryKey
  @NonNull
  @ColumnInfo(name = "Name")
  private final String mName;
  @ColumnInfo(name = "ingredient")
  protected final ArrayList<String> mIng;
  @ColumnInfo(name = "Image")
  private final String mImgPath;
  @ColumnInfo(name = "Info")
  private final String mInfo;
  @ColumnInfo(name = "id")
  private final String mId;
  @ColumnInfo(name = "Measure")
  protected final ArrayList<String> mMeasure;
  @ColumnInfo(name = "Alcohol")
  private final String mAlcohol;

  public Entry(@NonNull String id, @NonNull String name, ArrayList<String> ing, ArrayList<String> measure, String imgPath, String info, String mAlcohol) {
    this.mId = id;
    this.mName = name;
    this.mIng = ing;
    this.mMeasure = measure;
    this.mImgPath = imgPath;
    this.mInfo = info;
    this.mAlcohol = mAlcohol;
  }

  @Ignore
  public Entry(@NonNull String id, @NonNull String name, String imgPath, String mAlcohol){
    this.mId = id;
    this.mName = name;
    this.mImgPath = imgPath;
    this.mIng = null;
    this.mInfo = null;
    this.mMeasure = null;
    this.mAlcohol = mAlcohol;
  }

  // getters and setters
  public String getName(){
    return this.mName;
  }
  public String getId(){
    return this.mId;
  }
  public String getInfo(){
    return this.mInfo;
  }
  public String getImgPath(){
    return this.mImgPath;
  }
  public ArrayList<String> getIngredients(){
    return this.mIng;
  }
  public ArrayList<String> getMeasure(){ return this.mMeasure;}
  public String getAlcohol(){return this.mAlcohol;}
}
