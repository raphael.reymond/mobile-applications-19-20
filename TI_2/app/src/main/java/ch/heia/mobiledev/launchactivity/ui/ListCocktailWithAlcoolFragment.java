package ch.heia.mobiledev.launchactivity.ui;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ch.heia.mobiledev.launchactivity.CocktailListWithAlcoholAdapter;
import ch.heia.mobiledev.launchactivity.R;
import ch.heia.mobiledev.launchactivity.data.Entry;
import ch.heia.mobiledev.launchactivity.viewModel.ListWithAlcoolViewModel;


@SuppressWarnings({"unchecked", "WeakerAccess", "unused"})
public class ListCocktailWithAlcoolFragment extends ListFragment implements CocktailListWithAlcoholAdapter.OnCocktailListener {
    private ListWithAlcoolViewModel listWithAlcoolViewModel;
    private CocktailListWithAlcoholAdapter mAdapter;
    List<Entry> cocktails = new ArrayList<>();
    private String mParam;
    private String mValue;
    private String mPath;
    private List<Entry> entry;

    // needed per default constructor
    public ListCocktailWithAlcoolFragment() {
        // doesn't do anything special
    }

    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // save parameter and value for network request
        savedInstanceState.putString("param", mParam);
        savedInstanceState.putString("value", mValue);
        // save list of cocktails when screen rotates
        savedInstanceState.putSerializable("cocktails", (Serializable) cocktails);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_cocktail_with, container, false);
        listWithAlcoolViewModel = ViewModelProviders.of(this).get(ListWithAlcoolViewModel.class);
        // observe list cocktails without alcohol
        mAdapter = new CocktailListWithAlcoholAdapter(this.getContext(), this);
        listWithAlcoolViewModel.getAllCocktailsWithAlcohol().observeForever(this::addCocktail);
        // set parameters for network request
        mPath = "filter.php";
        mParam = "a";
        mValue = "Alcoholic";

        // set cocktails if list cocktails is not empty
        if(cocktails.size() != 0){
            mAdapter.setCocktails(cocktails);
        }

        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = Objects.requireNonNull(getActivity()).findViewById(R.id.recyclerviewwith);

        // check if list of cocktails is empty and connection to network is available
        if(cocktails.size() == 0) {
            if (isConnectionEnable()) {
                // do the network request
                listWithAlcoolViewModel.fetchData(this, mPath, mParam, mValue);
                // lambda to observe list of cocktails
                listWithAlcoolViewModel.getResponse().observe(this, response -> {
                    entry = response.getEntries();
                    // set informations about cocktails to view model
                    for (int i = 0; i < entry.size(); i++) {
                        listWithAlcoolViewModel.setName(entry.get(i).getId(), entry.get(i).getName(), entry.get(i).getIngredients(), entry.get(i).getMeasure(), entry.get(i).getImgPath(), entry.get(i).getInfo(), mValue);
                    }
                    // add cocktails to list used for screen rotation
                    cocktails.clear();
                    cocktails = mAdapter.getEntry();
                });
            }
        }
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
    }

    // method used when screen rotation
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            // when screen has rotate, get saved cocktails list without new network request
            cocktails = (List<Entry>) bundle.getSerializable("cocktails");
        }
    }
    // method used when user clicks on an entry in the list
    public void onCocktailClick(int position, View view) {
        Entry entry = listWithAlcoolViewModel.getEntry(position);
        // Create new fragment and transaction
        ListCocktailWithAlcoolFragmentDirections.ActionListCocktailWithAlcoolFragmentToCocktailFragment action = ListCocktailWithAlcoolFragmentDirections.actionListCocktailWithAlcoolFragmentToCocktailFragment(entry);
        action.setEntry(entry);
        Navigation.findNavController(view).navigate(action);
    }
    private void addCocktail(List<Entry> Entry) {
        mAdapter.setCocktails(Entry);
    }

    // verify if connection to network is available
    private boolean isConnectionEnable() {
        ConnectivityManager connectivityManager = ((ConnectivityManager) Objects.requireNonNull(getContext()).getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
