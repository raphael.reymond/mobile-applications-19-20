package ch.heia.mobiledev.launchactivity.network;

import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

class NetworkUtils {
  // data members
  // for logging
  private static final String TAG = NetworkUtils.class.getSimpleName();

  // constant strings that are URL related
  // the defininition depends on each web API
  private static final String API_HOST = "https://www.thecocktaildb.com/api/json/v1/1";


  // Retrieves the proper URL to query the web API
  // if you received query parameters from other parts of the application
  // you must pass these parameters as arguments to this method
  static URL getUrl(String path, String param, String value) {
    return buildUrlWithQueryParameters(path, param, value);
  }

  // Build the url for specific query parameters
  private static URL buildUrlWithQueryParameters(String path, String param, String value) {
    Uri queryUri = Uri.parse(API_HOST).buildUpon().
      appendPath(path)
            .appendQueryParameter(param, value)
                    .build();
    // use the Uri class for building the uri to be used
	// (using parse()/buildUpon()/appendXXX() methods)
    try {
	  URL queryUrl = new URL(queryUri.toString());
	  Log.v(TAG, "" +queryUrl);
      return queryUrl;
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
      return null;
    }
  }

  // called for getting the response from a specific URL
  static String getResponseFromHttpUrl(URL url) throws IOException {
    // use a HttpURLConnection instance for getting the response from the URL
    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
    try {
      urlConnection.setRequestMethod("GET");
      urlConnection.setRequestProperty("Accept", "application/json");
      InputStream in = urlConnection.getInputStream();

      Scanner scanner = new Scanner(in);
      scanner.useDelimiter("\\A");

      boolean hasInput = scanner.hasNext();
      String response = null;
      if (hasInput) {
        response = scanner.next();
      }
      scanner.close();
      return response;
    }
    finally {
      urlConnection.disconnect();
    }
  }
}
