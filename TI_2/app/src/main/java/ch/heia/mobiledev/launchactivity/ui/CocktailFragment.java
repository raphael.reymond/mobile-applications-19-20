package ch.heia.mobiledev.launchactivity.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import java.io.InputStream;
import java.util.Objects;

import ch.heia.mobiledev.launchactivity.R;
import ch.heia.mobiledev.launchactivity.data.Entry;
import ch.heia.mobiledev.launchactivity.viewModel.CocktailViewModel;

@SuppressWarnings("WeakerAccess")
public class CocktailFragment extends Fragment {
    private CocktailViewModel cocktailViewModel;
    private Entry mEntry;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.coktail_fragment, container, false);
        Bundle bundle = getArguments();
        assert bundle != null;
        mEntry = (Entry) bundle.getSerializable("entry");
        cocktailViewModel = ViewModelProviders.of(this).get(CocktailViewModel.class);

        return view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // display field for display later informations about the cocktail
        TextView cName = view.findViewById(R.id.NameCocktail);
        TextView cInfo = view.findViewById(R.id.InfoCocktail);
        TextView cIngre1 = view.findViewById(R.id.Ingre1);
        TextView cIngre2 = view.findViewById(R.id.Ingre2);
        TextView cIngre3 = view.findViewById(R.id.Ingre3);
        TextView cIngre4 = view.findViewById(R.id.Ingre4);

        // get info from API when connection is enabled
        if(isConnectionEnable()){
            // do the network request to have all the informations about the cocktail with id i
            cocktailViewModel.fetchData(this, "lookup.php", "i", mEntry.getId());

            // observe the reponse
            cocktailViewModel.getResponse().observe(getViewLifecycleOwner(), response -> {
                mEntry = response.getEntries().get(0);
                cName.setText(mEntry.getName());
                cInfo.setText(mEntry.getInfo());
                // download new image from cocktail
                if(mEntry.getImgPath()!=null){
                    new DownloadImageTask(view.findViewById(R.id.ImgCocktail))
                            .execute(mEntry.getImgPath());
                }
                // set the ingredients of the cocktail
                    cIngre1.setText(mEntry.getMeasure().get(0) + " " + mEntry.getIngredients().get(0));
                    cIngre2.setText(mEntry.getMeasure().get(1) + " " + mEntry.getIngredients().get(1));
                    cIngre3.setText(mEntry.getMeasure().get(2) + " " + mEntry.getIngredients().get(2));
                    if(mEntry.getMeasure().get(3) == null){
                        cIngre4.setText(mEntry.getMeasure().get(3) + " " + mEntry.getIngredients().get(3));
                    }
                    else{
                        cIngre4.setText(mEntry.getIngredients().get(3));
                    }

            });
        }

        // else get informations in the db
        else{
            cName.setText(mEntry.getName());
            cInfo.setText("For info please active the wifi");
            if(mEntry.getIngredients() != null){
                cIngre1.setText(mEntry.getMeasure().get(0) + " " + mEntry.getIngredients().get(0));
                cIngre2.setText(mEntry.getMeasure().get(1) + " " + mEntry.getIngredients().get(1));
                cIngre3.setText(mEntry.getMeasure().get(2) + " " + mEntry.getIngredients().get(2));
                cIngre4.setText(mEntry.getMeasure().get(3) + " " + mEntry.getIngredients().get(3));
            }
        }
    }


    @SuppressLint("StaticFieldLeak")
    private static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        final ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    private boolean isConnectionEnable() {
        ConnectivityManager connectivityManager = ((ConnectivityManager) Objects.requireNonNull(getContext()).getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
