package ch.heia.mobiledev.launchactivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;
import java.util.List;

import ch.heia.mobiledev.launchactivity.data.Entry;

@SuppressWarnings("unused")
public class CocktailListWithAlcoholAdapter extends RecyclerView.Adapter<CocktailListWithAlcoholAdapter.CocktailViewHolder> {
    private final Context mContext;
    private final LayoutInflater mInflater;
    private List<Entry> mCocktailsList = new LinkedList<>(); // Cached copy of names
    private final OnCocktailListener mOnCocktailListener;

    public CocktailListWithAlcoholAdapter(Context context, OnCocktailListener onCocktailListener) {
        this.mOnCocktailListener = onCocktailListener;
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
    }


    @NonNull
    @Override
    public CocktailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new CocktailViewHolder(itemView, mOnCocktailListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CocktailViewHolder holder, int position) {
        if (mCocktailsList != null) {
            Entry current = mCocktailsList.get(position);
            holder.cocktailItemView.setText(current.getName());
        } else {
            // Covers the case of data not being ready yet.
            holder.cocktailItemView.setText(mContext.getResources().getString(R.string.no_cocktail));
        }
    }
    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return
    // null).
    @Override
    public int getItemCount() {
        // account gracefully for the possibility that the data is not yet ready
        // and mNames is still null
        return mCocktailsList.size();
    }

    static class CocktailViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView cocktailItemView;
        final OnCocktailListener onCocktailListener;

        private CocktailViewHolder(View itemView, OnCocktailListener onCocktailListener) {
            super(itemView);
            cocktailItemView = itemView.findViewById(R.id.textView);
            this.onCocktailListener = onCocktailListener;

            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            onCocktailListener.onCocktailClick(getAdapterPosition(), v);
        }
    }
    public interface OnCocktailListener{
        void onCocktailClick(int position, View v);
    }
    public List<Entry> getEntry(){
        return this.mCocktailsList;
    }

    public void setCocktails(List<Entry> entry){
        mCocktailsList = entry;
        notifyDataSetChanged();
    }
}